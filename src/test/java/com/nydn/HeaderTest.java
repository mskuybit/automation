package com.nydn;

import com.nydn.apps.wrappers.BasePage;
import com.nydn.apps.wrappers.BaseTest;
import org.testng.annotations.Test;

public class HeaderTest extends BaseTest {

    @Test(groups = "smoke")
    public void openHomePage() {
        homePage().openPage()
                .verifyHomePageOpened();
    }

    @Test(groups = "smoke")
    public void verifyAdvertisementIsDisplayed() {
        homePage().openPage()
                .verifyAdvertisementIsDisplayed();
    }
}
