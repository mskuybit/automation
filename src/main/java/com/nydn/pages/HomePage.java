package com.nydn.pages;

import com.codeborne.selenide.SelenideElement;
import com.nydn.apps.wrappers.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;
import static com.nydn.apps.utils.properties.SystemProperty.BASE_URL;

public class HomePage extends BasePage<HomePage> {
    @Override
    protected String getUrl() {
        return BASE_URL.getValue();
    }

    public HomePage verifyHomePageOpened() {
        $("#nydailynews").isDisplayed();
        return this;
    }

    public HomePage verifyAdvertisementIsDisplayed() {
        SelenideElement element = $(".hp-aside .r-ad");
                element.scrollTo();
                element.$(By.tagName("iframe")).isDisplayed();
        return this;
    }
}

/*git@bitbucket.org:mskuybit/automation.git*/
