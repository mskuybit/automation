package com.nydn.apps.wrappers;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.nydn.apps.internal.GenericPage;
import com.nydn.apps.internal.TestCase;
import com.nydn.apps.listeners.EventListener;
import com.nydn.apps.listeners.Highlighter;
import com.nydn.apps.listeners.ScreenshotListener;
import com.nydn.apps.listeners.TestListener;
import com.nydn.apps.utils.report.AllurePropertiesUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.codeborne.selenide.WebDriverRunner.addListener;
import static java.lang.String.format;
import static org.apache.commons.collections.MapUtils.isEmpty;

/*
TODO add your name and date create this class
 */

@Listeners({ScreenshotListener.class, TestListener.class})
public abstract class BaseTest implements TestCase {

    private static final Logger logger = LogManager.getLogger(BaseTest.class);

    private static final ThreadLocal<Map<GenericPage, BasePage>> PAGES = new ThreadLocal<Map<GenericPage, BasePage>>() {
        public Map<GenericPage, BasePage> initialValue() {
            return new HashMap<>();
        }
    };

    private static AtomicInteger testsCounter = new AtomicInteger(0);

//    protected WebDriver driver;

    private static void logExecutionOrder(final Method method) {
        LOGGER.info(format("Executing test #%d: {%s, %s}", testsCounter.incrementAndGet(),
                method.getDeclaringClass().getSimpleName(), method.getName()));
    }

    public static WebDriver getDriver() {
        return WebDriverRunner.getWebDriver();
    }

    public static Map<GenericPage, BasePage> getPages() {
        return PAGES.get();
    }

    private static void configureDriverBaseOnParams() {
        getDriver().manage().window().maximize();
        Selenide.clearBrowserCookies();
    }

    @BeforeSuite(alwaysRun = true)
    public void setUp() {
        addListener(new EventListener());
        addListener(new Highlighter());
    }

    @BeforeMethod(alwaysRun = true)
    public static void configureBrowserBeforeTest(Method testMethod) {
        configureDriverBaseOnParams();
        logExecutionOrder(testMethod);
    }

    /**
     * Cleaning collection Pages
     */
    private void cleanUpPages() {
        if (!isEmpty(getPages())) {
            PAGES.remove();
        }
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        cleanUpPages();
    }

    /**
     * Creates the allure properties for the report, after the test run
     */
    @AfterSuite(alwaysRun = true)
    public static void createAllureProperties() {
        AllurePropertiesUtil.create();
    }

}



