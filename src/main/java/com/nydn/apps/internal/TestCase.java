package com.nydn.apps.internal;

public interface TestCase extends PageObjectsSupplier, Cookies, BrowserConsole {
}
