package com.nydn.apps.internal;

import com.nydn.apps.wrappers.BasePage;
import com.nydn.pages.HomePage;

import static com.nydn.apps.internal.PageObjectsSupplier.PageObject.HOME_PAGE;

public interface PageObjectsSupplier<T extends PageObjectsSupplier<T>> {

    default HomePage homePage() {
        return (HomePage) GenericPage.getPageObject(HOME_PAGE);
    }

    enum PageObject implements GenericPage {
        HOME_PAGE {
            public BasePage create() {
                return new HomePage();
            }
        }
    }
}
