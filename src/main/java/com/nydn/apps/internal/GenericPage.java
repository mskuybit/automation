package com.nydn.apps.internal;

import com.nydn.apps.wrappers.BasePage;

import static com.nydn.apps.wrappers.BaseTest.getPages;

public interface GenericPage {

    static BasePage getPageObject(final GenericPage page) {
        getPages().putIfAbsent(page, page.create());
        return getPages().get(page);
    }

    BasePage create();
}
