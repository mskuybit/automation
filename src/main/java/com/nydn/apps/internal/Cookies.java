package com.nydn.apps.internal;

import com.nydn.apps.wrappers.BaseTest;
import io.qameta.allure.Step;
import org.apache.log4j.Logger;
import org.openqa.selenium.Cookie;

import static com.nydn.apps.wrappers.BaseTest.getDriver;
import static java.lang.String.format;

/*

 */

public interface Cookies {
    Logger LOGGER = Logger.getLogger(Cookies.class);

    default void addCoockies(String cookiesName, String cookiesValue) {
        Cookie ck = new Cookie(cookiesName, cookiesValue);
        BaseTest.getDriver().manage().addCookie(ck);
        LOGGER.info(format("add cookies with name = \"%s\", value = \"%s\""));
    }

    /**
     * Deleting the specific cookie with cookie name
     * @param cookieName Cookie name "--utmb"
     */
    @Step("Deleting the specific cookie with cookie name \"{0}\"")
    default void deleteCookieNamed(final String cookieName) {
        getDriver().manage().deleteCookieNamed(cookieName);
    }


}
